package com.cleartrip.post.booking.communication.util;

import com.cleartrip.post.booking.communication.model.EmailPayload;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public abstract class CommunicationUtil {

        public static final ObjectMapper mapper = new ObjectMapper();

        public static String getObjToJsonString(Object properties) {

                try {
                        return mapper.writeValueAsString(properties);
                } catch (JsonProcessingException e) {
                        e.printStackTrace();
                        log.error(" JsonProcessingException " + e.getMessage());
                }

                return null;
        }

        /**
         * Object content : Content of mail if useTemplate = false in string format. if
         * useTemplate=true then Property Object for the placeholder in key value pair
         *
         */
        public EmailPayload getEmaileRequestObject(List<String> to, String from, String subject, Object content, String templateName,
                Boolean useTemplate, List<String> attachment) {

                EmailPayload email = new EmailPayload();
                email.setFrom(from);
                email.setTo(to);
                email.setSubject(subject);
                email.setAttachment(attachment);
                email.setUseTemplate(useTemplate);
                email.setCategory(templateName);
                if (useTemplate) {
                        email.setMailContent(getObjToJsonString(content));
                } else {
                        email.setMailContent((String) content);
                }

                return email;
        }
}
