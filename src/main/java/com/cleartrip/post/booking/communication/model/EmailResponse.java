package com.cleartrip.post.booking.communication.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(includeFieldNames = true)
public class EmailResponse {

        private String id;
        private String status;

        public void setId(String id) {
                this.id = id;
        }

        public String getId() {
                return id;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

}
