package com.cleartrip.post.booking.communication.model;

import com.cleartrip.post.booking.communication.constants.Constants.PdfStatus;
import lombok.Data;

@Data
public class PdfResponse {

        private String id;
        private PdfStatus status;
        private String reason;

        public String getId() {
                return id;
        }

        public void setId(String id) {
                this.id = id;
        }

        public PdfStatus getStatus() {
                return status;
        }

        public void setStatus(PdfStatus status) {
                this.status = status;
        }

        public String getReason() {
                return reason;
        }

        public void setReason(String reason) {
                this.reason = reason;
        }

        public String getPdfFileName() {
                return pdfFileName;
        }

        public void setPdfFileName(String pdfFileName) {
                this.pdfFileName = pdfFileName;
        }

        private String pdfFileName;
}

