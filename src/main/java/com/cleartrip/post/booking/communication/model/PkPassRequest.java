package com.cleartrip.post.booking.communication.model;
import lombok.Data;

@Data
public class PkPassRequest {

        private String id;
        private String pass;
        private String name;

}