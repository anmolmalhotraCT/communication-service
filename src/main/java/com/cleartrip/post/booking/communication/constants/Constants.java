package com.cleartrip.post.booking.communication.constants;

public interface Constants {
        enum AppCodes {
                OK(200),
                UNAUTHORISED(1000),
                FORBIDDEN(1001),
                BAD_REQUEST(400),
                INTERNAL_SERVER_ERROR(500);

                private Integer code;

                AppCodes(Integer code) {
                        this.code = code;
                }

                public Integer getCode() {
                        return code;
                }
        }

        enum EmailType {
                PROMOTIONAL("PROMOTIONAL"),
                TRANSACTIONAL("TRANSACTIONAL");
                private String value;

                private EmailType(String value) {
                        this.value = value;
                }
        }


        enum PdfStatus {
                SUCCESS,
                ACCEPTED,
                FAIL
        }
}
