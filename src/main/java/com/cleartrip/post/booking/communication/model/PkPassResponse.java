package com.cleartrip.post.booking.communication.model;
import com.cleartrip.post.booking.communication.constants.Constants.PdfStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PkPassResponse {


        private String id;
        private PdfStatus status;
        private String reason;
        private String pkPassFileName;

}
