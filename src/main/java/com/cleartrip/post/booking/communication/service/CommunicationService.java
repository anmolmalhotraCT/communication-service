package com.cleartrip.post.booking.communication.service;

import com.cleartrip.post.booking.communication.constants.Constants.EmailType;;
import com.cleartrip.post.booking.communication.exceptions.ApplicationException;
import com.cleartrip.post.booking.communication.model.*;

public interface CommunicationService {

        EmailResponse sendEmail(EmailPayload email, EmailType emailType) throws ApplicationException;

        SMSResponse sendSMS(SMSPayLoad sms) throws ApplicationException;

        PdfResponse generatePdf(PdfRequest pdf) throws ApplicationException;

        PkPassResponse generatePkPass(PkPassRequest pkPass);

}

