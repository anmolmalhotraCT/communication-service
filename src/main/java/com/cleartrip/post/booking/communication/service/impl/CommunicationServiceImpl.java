package com.cleartrip.post.booking.communication.service.impl;

import com.cleartrip.post.booking.communication.service.CommunicationService;
import com.cleartrip.post.booking.communication.exceptions.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.cleartrip.post.booking.communication.constants.Constants.EmailType;
import com.cleartrip.post.booking.communication.constants.Constants.PdfStatus;
import com.cleartrip.post.booking.communication.model.EmailPayload;
import com.cleartrip.post.booking.communication.model.EmailResponse;
import com.cleartrip.post.booking.communication.model.PdfRequest;
import com.cleartrip.post.booking.communication.model.PdfResponse;
import com.cleartrip.post.booking.communication.model.PkPassRequest;
import com.cleartrip.post.booking.communication.model.PkPassResponse;
import com.cleartrip.post.booking.communication.model.SMSPayLoad;
import com.cleartrip.post.booking.communication.model.SMSResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CommunicationServiceImpl implements CommunicationService {

        @Autowired
        Environment env;

        @Autowired
        RestTemplate restTemplate;

        @Override
        public EmailResponse sendEmail(EmailPayload email, EmailType emailType) throws ApplicationException {

                String url = null;
                EmailResponse emailResponse = null;
                if (emailType.equals(EmailType.PROMOTIONAL)) {
                        url = env.getProperty("ct.communication.service.email.promotional");
                } else if (emailType.equals(EmailType.TRANSACTIONAL)) {
                        url = env.getProperty("ct.communication.service.email.transactional");
                }
                log.info("sending email with payload : {} ", email);
                HttpEntity<EmailPayload> entity = new HttpEntity<EmailPayload>(email, getJsonHeader());
                try {
                        ResponseEntity<EmailResponse> response = restTemplate.postForEntity(url, entity, EmailResponse.class);
                        log.info("mail send to {} with response {} ", email.getTo(), response);
                        emailResponse = response.getBody();
                } catch (HttpStatusCodeException ex) {
                        log.error("Email HttpStatusCodeException : {} \n response is {}", ex, ex.getResponseBodyAsString());
                } catch (ResourceAccessException rae) {
                        log.error("Email ResourceAccessException {} for request {}", rae, email);
                        return retryPost(url, entity, EmailResponse.class, 2, 1);
                } catch (Exception e) {
                        log.error("Email Exception : {} ", e);
                }

                return emailResponse;
        }

        @Override
        public SMSResponse sendSMS(SMSPayLoad sms) throws ApplicationException {

                String smsServiceUrl = env.getProperty("ct.communication.service.sms");
                HttpEntity<SMSPayLoad> smsHttpEntity = new HttpEntity<>(sms, getJsonHeader());
                log.info("sending sms: {} to {}", sms.getContent(), sms.getMobileNumber());
                try {
                        return restTemplate.postForObject(smsServiceUrl, smsHttpEntity, SMSResponse.class);
                } catch (HttpStatusCodeException ex) {
                        log.error("SMSPayLoad HttpStatusCodeException : {} \n response is {}", ex, ex.getResponseBodyAsString());
                } catch (ResourceAccessException rae) {
                        log.error("sendSMS ResourceAccessException {} for request {}", rae, sms);
                        return retryPost(smsServiceUrl, smsHttpEntity, SMSResponse.class, 2, 1);
                } catch (Exception e) {
                        log.error("Sms Exception : ", e);
                }

                return null;
        }

        @Override
        public PdfResponse generatePdf(PdfRequest pdfRequest) throws ApplicationException {

                String generatePdfUrl = env.getProperty("ct.communication.service.generate.pdf");
                PdfResponse pdfResponse = null;
                HttpEntity<PdfRequest> pdfHttpEntity = new HttpEntity<>(pdfRequest, getJsonHeader());
                log.info(" Going to create pdf with request {} ", pdfRequest);
                try {
                        ResponseEntity<PdfResponse> entity = restTemplate.postForEntity(generatePdfUrl, pdfHttpEntity, PdfResponse.class);
                        log.info("Response recieve after request {}", entity);
                        if (entity.getStatusCode() == HttpStatus.OK) {
                                pdfResponse = entity.getBody();
                                if (PdfStatus.FAIL.equals(pdfResponse.getStatus())) {
                                        log.error("PdfResponse got error : {}", pdfResponse);
                                        return null;
                                }
                                return pdfResponse;
                        }
                } catch (HttpStatusCodeException ex) {
                        log.error("PdfRequest HttpStatusCodeException : {} \n response is {}", ex, ex.getResponseBodyAsString());
                } catch (ResourceAccessException rae) {
                        log.error("PdfRequest ResourceAccessException {}", rae);
                        return retryPost(generatePdfUrl, pdfHttpEntity, PdfResponse.class, 2, 1);
                } catch (Exception e) {
                        log.error("Pdf Exception : ", e);
                        log.error("Pdf Request {} : Pdf Response {}", pdfRequest, pdfResponse);
                }

                return pdfResponse;
        }

        @Override
        public PkPassResponse generatePkPass(PkPassRequest pkPass) {

                String url = env.getProperty("ct.communication.service.email.pkpass");
                PkPassResponse pkPassResponse = null;
                log.info("Going to generate pkpass with payload : {} ", pkPass);
                HttpEntity<PkPassRequest> entity = new HttpEntity<PkPassRequest>(pkPass, getJsonHeader());
                try {
                        ResponseEntity<PkPassResponse> response = restTemplate.postForEntity(url, entity, PkPassResponse.class);
                        pkPassResponse = response.getBody();
                } catch (HttpStatusCodeException ex) {
                        log.error("PkPass HttpStatusCodeException : {} \n response is {}", ex, ex.getResponseBodyAsString());
                } catch (ResourceAccessException rae) {
                        log.error("PkPass ResourceAccessException {}", rae);
                        return retryPost(url, entity, PkPassResponse.class, 2, 1);
                } catch (Exception e) {
                        log.error("PkPass Exception : {} ", e);
                }
                log.info("PkPass response {} ", pkPassResponse);
                return pkPassResponse;
        }

        public <T> T retryPost(String url, HttpEntity<?> entity, Class<T> cls, int maxRetry, int retryCount) {

                if (maxRetry < retryCount) {
                        log.info("Retried max allowed time hence returning back for req {}", entity);
                        return null;
                }
                log.info("Going to retry count {} and allowed retry is {}", retryCount, maxRetry);
                try {
                        ResponseEntity<T> response = restTemplate.postForEntity(url, entity, cls);
                        return response.getBody();
                } catch (HttpStatusCodeException ex) {
                        log.error("Retry Logic failed with HttpStatusCodeException : {} \n response is {}", ex, ex.getResponseBodyAsString());
                } catch (Exception e) {
                        log.error("Retry Logic Exception count {} : {} ", retryCount, e);
                        return retryPost(url, entity, cls, maxRetry, retryCount + 1);
                }

                return null;
        }

        /**
         * @return
         */
        private HttpHeaders getJsonHeader() {
                return new HttpHeaders() {
                        {
                                set("Content-Type", "application/json");
                        }
                };
        }

}
