package com.cleartrip.post.booking.communication.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(includeFieldNames = true)
public class PdfRequest {

        private String id;
        private String templateName;
        private String pdfName;

        public String getId() {
                return id;
        }

        public void setId(String id) {
                this.id = id;
        }

        public String getTemplateName() {
                return templateName;
        }

        public void setTemplateName(String templateName) {
                this.templateName = templateName;
        }

        public String getPdfName() {
                return pdfName;
        }

        public void setPdfName(String pdfName) {
                this.pdfName = pdfName;
        }

        public String getTemplateContent() {
                return templateContent;
        }

        public void setTemplateContent(String templateContent) {
                this.templateContent = templateContent;
        }

        private String templateContent;

}
