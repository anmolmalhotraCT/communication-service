/**
 * 
 */
package com.cleartrip.post.booking.communication.exceptions;

import com.cleartrip.post.booking.communication.constants.Constants.AppCodes;

import java.io.IOException;

public class ApplicationException extends IOException {

    private static final long serialVersionUID = 1L;

    private AppCodes errorCode;

    private String message;

    public ApplicationException(AppCodes errCode, String message) {
        this.errorCode = errCode;
        this.message = message;
    }

    /**
     * @return the errorCode
     */
    public AppCodes getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode
     *            the errorCode to set
     */
    public void setErrorCode(AppCodes errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
