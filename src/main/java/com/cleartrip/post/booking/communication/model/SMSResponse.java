package com.cleartrip.post.booking.communication.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(includeFieldNames = true)
public class SMSResponse {

        private String status;
        private String smsId;
        private String message;


        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public String getSmsId() {
                return smsId;
        }

        public void setSmsId(String smsId) {
                this.smsId = smsId;
        }

        public String getMessage() {
                return message;
        }

        public void setMessage(String message) {
                this.message = message;
        }



}
