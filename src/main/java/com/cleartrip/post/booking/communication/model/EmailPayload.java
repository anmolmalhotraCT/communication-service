package com.cleartrip.post.booking.communication.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString(includeFieldNames = true)
public class EmailPayload {
        /*
         * optional field, tracking id for tracking
         */
        private String id;
        private String from;
        /* Communication Service should use template or not */
        private Boolean useTemplate;
        /*
         * Template name, if useTemplate=true
         */
        private String category;

        private String subject;
        /*
         * Content of mail if useTemplate = false. if useTemplate=true then json
         * containing the placeholder key value pair in string format
         */
        private String mailContent;
        private List<String> to;
        // optional field
        private List<String> cc;
        // optional field
        private List<String> bcc;
        // optional field, Attachment names
        private List<String> attachment;

        private String emailStatus;

}
