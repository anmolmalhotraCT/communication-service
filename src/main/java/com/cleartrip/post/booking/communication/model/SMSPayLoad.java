package com.cleartrip.post.booking.communication.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString(includeFieldNames = true)
public class SMSPayLoad {

        private String mobileNumber;
        private String content;

        /*
         * optional, it is tracking-id basically used for tracking purpose
         */
        private String ccId;

        /*
         * optional, description required
         */
        private String type;

        public String getMobileNumber() {
                return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
                this.mobileNumber = mobileNumber;
        }

        public String getContent() {
                return content;
        }

        public void setContent(String content) {
                this.content = content;
        }

        public String getCcId() {
                return ccId;
        }

        public void setCcId(String ccId) {
                this.ccId = ccId;
        }

        public String getType() {
                return type;
        }

        public void setType(String type) {
                this.type = type;
        }

        public List<String> getTags() {
                return tags;
        }

        public void setTags(List<String> tags) {
                this.tags = tags;
        }

        /*
         * optional, list of tags
         */
        private List<String> tags;
}
